#!/usr/bin/env python3

from diagrams import Diagram, Cluster
from diagrams.onprem.container import Docker
from diagrams.onprem.client import User

with Diagram("Listes services", direction="TB", show=False):
    # transmission = Docker("transmission.127.0.0.1.nip.io")
    # jackett = Docker("jackett.127.0.0.1.nip.io")
    # radarr = Docker("radarr.127.0.0.1.nip.io")
    # sonarr = Docker("sonarr.127.0.0.1.nip.io")
    # jellyfin = Docker("jellyfin.127.0.0.1.nip.io")
    # portainer = Docker("portainer.127.0.0.1.nip.io")
    # bazarr = Docker("bazarr.127.0.0.1.nip.io")
    # nzbhydra = Docker("nzbhydra.127.0.0.1.nip.io")
    traefik = Docker("traefik")
    user = User("User")
    with Cluster("Services Reverse Proxy Exposés"):
        reverse = [
            Docker("transmission"),
            Docker("jackett"),
            Docker("radarr"),
            Docker("sonarr"),
            Docker("jellyfin"),
            Docker("portainer"),
            Docker("bazarr"),
            Docker("nzbhydra"),
        ]
    user >> traefik >> reverse
