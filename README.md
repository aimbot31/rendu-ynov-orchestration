# Rendu Orchestration

Une archive (`zippade.zip`) est disponible à la racine du projet dans le cas où
il y est un problème avec les submodules de git.

## TP Media

![Liste Services](schema/listes_services.png)

### Exemple d'une requete sur un service

![Diagrame Sequence](schema/tp-media%20sequence.png)

### Liste des services

- [organizr.176.158.16.216.nip.io](https://organizr.176.158.16.216.nip.io)
- [nzbhydra.176.158.16.216.nip.io](https://nzbhydra.176.158.16.216.nip.io)
- [bazarr.176.158.16.216.nip.io](https://bazarr.176.158.16.216.nip.io)
- [portainer.176.158.16.216.nip.io](https://portainer.176.158.16.216.nip.io)
- [jellyfin.176.158.16.216.nip.io](https://jellyfin.176.158.16.216.nip.io)
- [sonarr.176.158.16.216.nip.io](https://sonarr.176.158.16.216.nip.io)
- [radarr.176.158.16.216.nip.io](https://radarr.176.158.16.216.nip.io)
- [jackett.176.158.16.216.nip.io](https://jackett.176.158.16.216.nip.io)
- [transmission.176.158.16.216.nip.io](https://transmission.176.158.16.216.nip.io)

### Traefik

#### Middlewares

Uitlisation du middleware de compression pour optimiser le temps de chargement
des pages compatibless.
Utilisation du middleware de redirection http vers https.

#### Certificats

Les certificats SSL configurés sont délivré par l'autorité de certification de
**staging** de let's encrypt. De ce fait, le navigateur ne reconnait pas le
certificat comme étant un certificat "valable".

### Réseaux

Les services sont tous dans un réseau à part pour une meilleur isolation et
un meilleur contrôle.

### Credentials

**jellyfin :**
- `admin`
- `adminadmin`

**organizr :**
- `admin`
- `adminadmin`

**transmission :**
- `username`
- `password`
